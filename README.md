# NGRX TODO

## Install dev

First, open a terminal, navigate to a directory of your choice and type :

`git clone https://bitbucket.org/webmastertoulouse/ngrx-todo`

After that, type `cd ngrx-todo && npm install` 

After all dependencies installed, type `ng serve` for launch angular api in dev mode.

Go to `http://localhost:4200/` in your browser.

_ngrx-todo is based on ngrx store library. All todo action (list, update, add) pass by store ngrx middleware._

*Build :*

`npm run-script build`

## Comments

*Bad news :*

* In this app, no material design (but bootstrap and fontawesome).
* No unit tests (missed time)
* No commit for all step (forget)

Apart from that, 

* You can list todos
* You can change state and have effect after change (todo at the bottom and crossed-out)
* You can add a todo
* You can access a todo by uniq url in a dedicated view
* Mocked backend has been created
* ngrx state management work fine (may be a bug after add a todo (api call 2 times, but no more time...)).
