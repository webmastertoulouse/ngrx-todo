import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import todosData from './../todos/todos-data';
import { Todo } from '../todos/models/todo.model';
 
@Injectable()
export class MockedBackendInterceptor implements HttpInterceptor {
 
  constructor() { }
 
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let todos: any[] = JSON.parse(localStorage.getItem('todos')) || [];
    if (todos.length == 0) {
      localStorage.setItem('todos', JSON.stringify(todosData));
      todos = JSON.parse(localStorage.getItem('todos')) || [];
    }
 
    return of(null).pipe(mergeMap(() => {
 
      // get todos
      if (request.url.endsWith('/todos') && request.method === 'GET') {
        return of(new HttpResponse({ status: 200, body: todos }));
      }
 
      // get todo by id
      if (request.url.match(/\/todos\/.*/) && request.method === 'GET') {
        let urlParts = request.url.split('/');
        let id = urlParts[urlParts.length - 1];
        let matchedTodos = todos.filter(todo => { return todo.id === id; });
        let todo = matchedTodos.length ? matchedTodos[0] : null;
 
        return of(new HttpResponse({ status: 200, body: todo }));
      }
 
      // register todo
      if (request.url.endsWith('/todos\/add') && request.method === 'POST') {
        let newTodo = request.body;
 
        let duplicateTodo = todos.filter(todo => { return todo.title === newTodo.title; }).length;
        if (duplicateTodo) {
          return throwError({ error: { message: 'Todo title "' + newTodo.title + '" is already taken' } });
        }
 
        newTodo.id = Math.random().toString(36).slice(2);
        todos.push(newTodo);
        localStorage.setItem('todos', JSON.stringify(todos));
 
        return of(new HttpResponse({ status: 200, body: newTodo }));
      }
 
      // delete todo
      if (request.url.match(/\/todos\/delete\/.*/) && request.method === 'DELETE') {
        let urlParts = request.url.split('/');
        let id = urlParts[urlParts.length - 1];
        for (let i = 0; i < todos.length; i++) {
          let todo = todos[i];
          if (todo.id === id) {
            todos.splice(i, 1);
            localStorage.setItem('todos', JSON.stringify(todos));
            break;
          }
        }

        return of(new HttpResponse({ status: 200 }));
      }
 
      // update todo by id
      if (request.url.match(/\/todos\/update\/.*/) && request.method === 'PATCH') {
        let updTodo = request.body;

        let urlParts = request.url.split('/');
        let id = urlParts[urlParts.length - 1];
        for (let i=0; i<todos.length; i++) {
          if (id == todos[i].id) {
            todos[i] = updTodo;
            localStorage.setItem('todos', JSON.stringify(todos));
            break;
          }
        }
 
        return of(new HttpResponse({ status: 200, body: updTodo }));
      }
 
      // update todo by id
      if (request.url.match(/\/todos\/toggle\/.*/) && request.method === 'PATCH') {
        let state = request.body;
        let todoToUpd: Todo;

        let urlParts = request.url.split('/');
        let id = urlParts[urlParts.length - 1];
        for (let i=0; i<todos.length; i++) {
          if (id == todos[i].id) {
            todos[i].state = state;
            todoToUpd = todos[i];
            localStorage.setItem('todos', JSON.stringify(todos));
            break;
          }
        }
 
        return of(new HttpResponse({ status: 200, body: todoToUpd }));
      }
 
      return next.handle(request);
       
    }))
 
    .pipe(materialize())
    .pipe(delay(500))
    .pipe(dematerialize());
  }
}
 
export let MockedBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: MockedBackendInterceptor,
  multi: true
};