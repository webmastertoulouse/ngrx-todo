export interface Todo {
  id: string;
  title: string;
  state: boolean;
  description?: string;
  date_add?: number;
}
