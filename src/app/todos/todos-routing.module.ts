import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoShowComponent } from './components/todo-show/todo-show.component';
import { TodoAddComponent } from './components/todo-add/todo-add.component';

const routes: Routes = [
  {
    path: '',
    component: TodoListComponent
  }, 
  {
    path: 'add',
    component: TodoAddComponent
  }, 
  {
    path: ':id',
    component: TodoShowComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodosRoutingModule { }
