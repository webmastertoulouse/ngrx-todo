import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Todo } from "../models/todo.model";
import { environment } from "src/environments/environment";

@Injectable()
export class TodoService {

  private _baseUrl: string = environment.baseURL;

  constructor(private _http: HttpClient) {}

  getTodos(): Observable<Todo[]> {
    return this._http.get<Todo[]>(`${this._baseUrl}todos`);
  }

  updateTodo(todo: Todo): Observable<Todo> {
    return this._http.patch<Todo>(`${this._baseUrl}todos/update/${todo.id}`, todo);
  }

  toggleState(id, state): Observable<Todo> {
    return this._http.patch<Todo>(`${this._baseUrl}todos/toggle/${id}`, state);
  }

  getTodo(id: number): Observable<Todo> {
    return this._http.get<Todo>(`${this._baseUrl}todos/${id}`);
  }

  addTodo(todo: Todo): Observable<Todo> {
    return this._http.post<Todo>(`${this._baseUrl}todos/add`, todo);
  }
}