import { Action } from '@ngrx/store';
import { Todo } from '../../models/todo.model';
import { ActionTypes } from './type.action';

export class GetTodos implements Action {
  readonly type = ActionTypes.GET_TODOS;
}

export class SetTodos implements Action {
  readonly type = ActionTypes.SET_TODOS;

  constructor(public payload: { todos: Todo[] }) {}
}

export class GetTodo implements Action {
  readonly type = ActionTypes.GET_TODO;

  constructor(public payload: { todo: Todo }) {}
}

export class AddTodo implements Action {
  readonly type = ActionTypes.ADD_TODO;

  constructor(public payload: { todo: Todo }) {}
}

export class AddTodoError implements Action {
  readonly type = ActionTypes.ADD_TODO_ERROR;

  constructor(public payload: any) {}
}

export class ToggleState implements Action {
  readonly type = ActionTypes.TOGGLE_STATE;

  constructor(public payload: { id: string, state: boolean }) {}
}

export class UdpateTodo implements Action {
  readonly type = ActionTypes.UPDATE_TODO;

  constructor(public payload: { todo: Todo }) {}
}

export class UdpateTodoSuccess implements Action {
  readonly type = ActionTypes.UPDATE_TODO_SUCCESS;
}

export type TodoActions = SetTodos | GetTodos | GetTodo | AddTodo | AddTodoError | ToggleState | UdpateTodo;