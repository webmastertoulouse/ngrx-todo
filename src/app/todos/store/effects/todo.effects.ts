import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { ActionTypes } from '../actions/type.action';
import { switchMap, map, toArray, flatMap, catchError, tap } from 'rxjs/operators';
import { TodoService } from '../../services/todo.service';
import * as TodoActions from './../../store/actions/todo.actions';
import { Todo } from '../../models/todo.model';
import { Router } from '@angular/router';
 
@Injectable()
export class TodoEffects {
 
  constructor(private _actions$: Actions,
    private _todoService: TodoService,
    private _router: Router) {}

  @Effect()
  getTodos$: Observable<Action> = this._actions$.pipe(
    ofType(ActionTypes.GET_TODOS),
    switchMap(() => {
      return this._todoService.getTodos()
        .pipe(
          flatMap((todos: any) => {
            return todos.sort((a, b) => a.state ? -1 : 0)
          }),
          toArray(),
          map((todos: any) => {
            return new TodoActions.SetTodos({ todos: todos })
          })
        )
    })
  );

  @Effect()
  toggleState$: Observable<Action> = this._actions$.pipe(
    ofType(ActionTypes.TOGGLE_STATE),
    map((action: any) => action.payload),
    switchMap((payload) => this._todoService.toggleState(payload.id, payload.state)),
    switchMap(res => [
      new TodoActions.UdpateTodoSuccess(),
      new TodoActions.GetTodos()
    ])
  );

  @Effect()
  addTodo$: Observable<Action> = this._actions$.pipe(
    ofType(ActionTypes.ADD_TODO),
    map((action: any) => action.payload),
    switchMap((payload) => {
      return this._todoService.addTodo(payload.todo)
        .pipe(
          map((todo: Todo) => new TodoActions.AddTodo({ todo: todo })),
          catchError(err => of(new TodoActions.AddTodoError(err)))
        )
    }),
    tap(() => {
      this._router.navigate(['/todos']);
    })
  );
}