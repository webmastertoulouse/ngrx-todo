import { createFeatureSelector, ActionReducerMap, createSelector } from '@ngrx/store';
import * as fromTodo from './todo.reducer';

export const reducers: ActionReducerMap<any> = {
  todo: fromTodo.todoReducer
};

export const selectTodoState = createFeatureSelector<fromTodo.State>('todos');

export const { selectAll: selectAllTodos } = fromTodo.todoAdapter.getSelectors(
  selectTodoState
);