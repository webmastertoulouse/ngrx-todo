import { createEntityAdapter, EntityState, EntityAdapter } from '@ngrx/entity';

import { Todo } from '../../models/todo.model';

import * as todoActions from '../actions/todo.actions';
import { ActionTypes } from '../actions/type.action';

export interface State extends EntityState<Todo> {}

export const todoAdapter: EntityAdapter<Todo> = createEntityAdapter<Todo>();

export const initialState: State = todoAdapter.getInitialState();

export function todoReducer(
  state: State = initialState,
  action: todoActions.TodoActions
) {
  switch (action.type) {
    case ActionTypes.SET_TODOS:
      return todoAdapter.addAll(action.payload.todos, state);
    case ActionTypes.ADD_TODO:
      return todoAdapter.addOne(action.payload.todo, state);
    // case ActionTypes.ADD_TODO_ERROR:
      // return action.payload;
    case ActionTypes.TOGGLE_STATE:
      return todoAdapter.updateOne(
        { id: action.payload.id, changes: { state: action.payload.state } },
        state
      );
    default:
      return state;
  }
}