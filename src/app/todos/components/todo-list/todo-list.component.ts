import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Todo } from './../../models/todo.model';
import * as TodoActions from './../../store/actions/todo.actions';
import { selectAllTodos } from './../../store/reducers';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { State } from '../../store/reducers/todo.reducer';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  todos$: Observable<Todo[]> = new Observable<Todo[]>();
  todo: Todo;

  constructor(private _store: Store<State>,
    private _router: Router) {}

  ngOnInit() {
    this._store.dispatch(new TodoActions.GetTodos());
    this.todos$ = this._store.select(selectAllTodos);
  }

  addTodo() {
    this._router.navigate(['/todos/add']);
  }

  showTodo(todo) {
    this._router.navigate(['/todos', todo.id]);
  }

  toggleState($event, todo) {
    $event.stopPropagation();
    todo.state = !todo.state;
    this._store.dispatch(new TodoActions.ToggleState({ id: todo.id, state: todo.state }));
  }
}
