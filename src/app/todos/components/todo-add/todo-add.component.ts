import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Todo } from '../../models/todo.model';
import { Store } from '@ngrx/store';
import * as TodoActions from '../../store/actions/todo.actions';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.scss']
})
export class TodoAddComponent {

  todoForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl(''),
    state: new FormControl(true)
  });

  constructor(private _route: ActivatedRoute,
    private _store: Store<any>) { }

  onSubmit() {
    let todo: Todo = Object.assign({}, this.todoForm.value);
    todo.date_add = Date.now();
    this._store.dispatch(new TodoActions.AddTodo({ todo }));
  }
}
