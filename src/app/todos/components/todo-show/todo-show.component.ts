import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TodoService } from '../../services/todo.service';
import { Todo } from '../../models/todo.model';
import { Store } from '@ngrx/store';
import * as TodoActions from '../../store/actions/todo.actions';

@Component({
  selector: 'app-todo-show',
  templateUrl: './todo-show.component.html',
  styleUrls: ['./todo-show.component.scss']
})
export class TodoShowComponent implements OnInit {

  public todo: Todo = {
    id: '',
    title: '',
    description: '',
    state: true
  };

  constructor(private _route: ActivatedRoute,
    private _todoService: TodoService,
    private _store: Store<any>) { }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this._todoService.getTodo(params.id)
        .subscribe((todo: Todo) => {
          this.todo = todo;
          this._store.dispatch(new TodoActions.GetTodo({ todo: todo }));
        });
    });
  }
}
