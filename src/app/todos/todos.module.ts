import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { todoReducer } from './store/reducers/todo.reducer';
import { TodosRoutingModule } from './todos-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { TodoService } from './services/todo.service';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoShowComponent } from './components/todo-show/todo-show.component';
import { TodoAddComponent } from './components/todo-add/todo-add.component';
library.add(fas);
import { FormsModule, ReactiveFormsModule } from "@angular/forms"; 
import { TodoEffects } from './store/effects/todo.effects';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [TodoListComponent, TodoShowComponent, TodoAddComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature('todos', todoReducer),
    EffectsModule.forRoot([TodoEffects]),
    TodosRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    TodoService
  ]
})
export class TodosModule { }
