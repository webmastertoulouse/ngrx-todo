import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  isMenuOpen: boolean = false;

  toggle() {
    this.isMenuOpen = !this.isMenuOpen;
  }
}
